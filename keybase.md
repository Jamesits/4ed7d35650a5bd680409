### Keybase proof

I hereby claim:

  * I am Jamesits on github.
  * I am jamesits (https://keybase.io/jamesits) on keybase.
  * I have a public key whose fingerprint is F37F 9BC8 BBE8 C9AB 7EC2  D7E3 5CE8 0C34 0ACC 0CC0

To claim this, I am signing this object:

```json
{
    "body": {
        "key": {
            "eldest_kid": "01017302463af2775ca2d1129904f52af7eaac984b3be82c1043333429b9b27258430a",
            "fingerprint": "f37f9bc8bbe8c9ab7ec2d7e35ce80c340acc0cc0",
            "host": "keybase.io",
            "key_id": "5ce80c340acc0cc0",
            "kid": "01017302463af2775ca2d1129904f52af7eaac984b3be82c1043333429b9b27258430a",
            "uid": "296d24977ff750086c9c6dbfca3d8419",
            "username": "jamesits"
        },
        "service": {
            "name": "github",
            "username": "Jamesits"
        },
        "type": "web_service_binding",
        "version": 1
    },
    "ctime": 1455173444,
    "expire_in": 157680000,
    "prev": "8eac808f09e453a287ebfa5d535af59b1bfdcfe266814c01f634e3424174d0bd",
    "seqno": 3,
    "tag": "signature"
}
```

with the key [F37F 9BC8 BBE8 C9AB 7EC2  D7E3 5CE8 0C34 0ACC 0CC0](https://keybase.io/jamesits), yielding the signature:

```
-----BEGIN PGP MESSAGE-----
Version: Keybase OpenPGP v2.0.49
Comment: https://keybase.io/crypto

yMIWAnicrZJbSFRRFIZnuomCktlbYHayIDDd55x9blOakBZJBPaQDxbTvo4n7cx4
zmiJ2MUKLYjuEBKmlYmUECJFNKGGlWFXukAWFYFdlCztoRep9khBQY9tNiz22v/3
s9Zi9SdP9yX5Fw2+z3a05QX+wb5DVb4NsZyCWgmHaY0UqJXK2VRgFZR50WC5TaWA
BGQgGypQoK4irhiGRpBCZVmxLAC5piBuMISIZUKsYmYqRAZQFQcqFrawYiiaCVWA
pCyJ206IuRHXdqLClqsGtzAxsYCIhbDBiEINpmqEmYCoECBCgLgCLAt7cUIUh5HH
su2wyIlHcKq8f+j/c91VU3aKpVMFWobBuaEBYOrEIjrFnCCVmlC24kKPuQ7ayoR6
iwieHfWkuixJZKttwuKD/fUbsqNlVfhvougPIloTiae2MRz8BQex7VAxP8FUM9ez
w44UkIWSRO04LUNNE81CCLMktj1iuyxoxxWaoZtAnCwp4rJqYWkyRExgcmAxqKlI
MQ2GOdKopmqIaxaWMaeEM0XXTRkSIHNdhUzMBMoGpABTKd5OpROWAqooE4WEpWeH
HBStcplUd6N34wyfP8k3a+a0+F75khJn/962s6GEH7Q4U0loOtFzs68k8cDA7Reo
ZGJRe170WumK5nPmiHwq8iGtrfOyczyntU55ebcllt57v6fx5LJvO3rTDwbS7V3n
EiYr7rW8bZ6XUvzwUv2VnXcqSieT58xfe/Vww+7xN27il9qgvqerPHc08/GZsfaR
lI/+jNTx5IyxNcU0p+l08OiRT+09R57znK+xB17+ZvfYqqUnLqzsGMgr3D3RObAh
//OT+dmx9h8dA+eNq6u7/JXaw1tDw6Vpzxb0jy7ZX95Nr9d+WD88tLih4wA1d6aB
0u5Grp7pe7r39VyrW3/ftKmoaF9h/YOhZ+8mri8cTM1oa2t9dPHu9/NmbknLo1ez
6tct/gkIZkI/
=b1pY
-----END PGP MESSAGE-----

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/jamesits

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id jamesits
```
